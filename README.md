# Install JSON Server
npm install -g json-server
# API ÇALIŞTIRMAK İÇİN 
npx json-server --watch db.json
# PROJEYİ ÇALIŞTIRMAK İÇİN
ng serve
# cannot find module package.json hatası çıkarsa
ng-module silinip npm install yapılır.
# command not found:ng
npm install -g @angular/cli@latest
# Angular CLI version
nvm install v14.15.5
nvm use v14.15.5


Veriler db.json belgesine kaydedilip, güncelleme yine bu belgede gerçekleştirildi.



# StockManagement

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
