import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'stock-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  id;
  subscriptions: Subscription = new Subscription();
  dialogOpen = false;
  dialogRef;
  productForm: FormGroup;
  @ViewChild('calladdProductDialog') calladdProductDialog: TemplateRef<any>;
  @ViewChild('callEditProductDialog') callEditProductDialog: TemplateRef<any>;
  @Output() addProductClicked = new EventEmitter<string>();
  @Output() editProductClicked = new EventEmitter<string>();
  @Output() updateProductValue = new EventEmitter<string>();


  columnNames: string[] = [
    'name',
    'stockQty',
    'brand',
    'category',
    'createdDate',
    'updatedDate',
    'edit'
  ];
  _displayedScore;
  @Input()
  set data(value: any) {
    if (!value) return;
    this.id = value[value.length - 1]['id'] + 1;
    (<FormControl>this.productForm.controls['id'])?.patchValue(this.id);
    this._displayedScore = new MatTableDataSource(value);

    this.data.filterPredicate = (data, filter: string) => {
        return data.name.toLowerCase() === filter || data.category.toLowerCase().includes(filter) || data.brand.toLowerCase().includes(filter) || data.stockQty.toString().includes(filter) || data.createdDate.includes(filter);
    
     };
     
    this._displayedScore.paginator = this.paginator;
    this._displayedScore.sort = this.sort;
  }

  get data() {
    return this._displayedScore;
  }
  _specificData;
  @Input()
  set specificData(value: any) {
    if (!value) return;
    this._specificData = value;
    (<FormControl>this.productForm.controls['name'])?.patchValue(value.name);
    Object.keys(this.productForm.value).map(key => {
      (<FormControl>this.productForm.controls[key])?.patchValue(this._specificData[key]);
    });

  }
  get specificData() {
    return this._specificData;
  }
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.data.filter = filterValue.trim().toLowerCase();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }
  constructor(private fb: FormBuilder, private dialog: MatDialog) {
    this.productForm = fb.group({
      id: '',
      name: [
        null,
        Validators.compose([
          Validators.maxLength(250),
          Validators.required
        ]),
      ],
      stockQty: '',
      brand: '',
      category: '',
      createdDate: '',
      updatedDate: '-'
    });
  }

  ngOnInit(): void {
   
  }
  addProductClick() {
    this.dialogRef = this.dialog.open(this.calladdProductDialog, {
      width: '500px'
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
  addFormValue() {
    (<FormControl>this.productForm.controls['createdDate'])?.patchValue(new Date().toLocaleString());
    this.addProductClicked.next(this.productForm.value);
    this.productForm.reset();

  }
  editProductValue() {
    (<FormControl>this.productForm.controls['updatedDate'])?.patchValue(new Date().toLocaleString());

    this.updateProductValue.next(this.productForm.value);
    this.productForm.reset();
  }
  editClick(event) {
    this.editProductClicked.next(event);
    this.dialogRef = this.dialog.open(this.callEditProductDialog, {
      width: '500px'
    });
    this.subscriptions.add(
      this.dialogRef.backdropClick().subscribe(() => {
        this.productForm.reset();
      })
    );
  }
}
