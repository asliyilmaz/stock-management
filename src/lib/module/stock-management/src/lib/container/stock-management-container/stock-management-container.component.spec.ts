import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockManagementContainerComponent } from './stock-management-container.component';

describe('StockManagementContainerComponent', () => {
  let component: StockManagementContainerComponent;
  let fixture: ComponentFixture<StockManagementContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockManagementContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockManagementContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
