import { Component, OnInit } from '@angular/core';
import { Stock } from '../../stock';
import { StockManagementService } from '../../stock-management.service';

@Component({
  selector: 'app-stock-management-container',
  templateUrl: './stock-management-container.component.html',
  styleUrls: ['./stock-management-container.component.scss']
})
export class StockManagementContainerComponent implements OnInit {
  stock: Stock[];
  specificData;
  constructor(private api: StockManagementService) { }

  ngOnInit(): void {
    this.updateTableValue();
  }
  updateTableValue(){
    this.api.getStocks().subscribe((data) => {
      this.stock = data;
    },
      (error) => {
        console.log("error msg", error)
      }
    );
  }
  handleAddFormValue(event) {
    this.api.postStocks(event);
    this.updateTableValue();

  }
  handleEditProductClicked(event){
    this.api.getSpecificId(event).subscribe((data)=>{
      this.specificData=data;
    });
  }
  handleUpdateProductValue(event){
    this.api.updateStocks(event).then(data=>{
      this.updateTableValue();
    });
    
    
  }

}
