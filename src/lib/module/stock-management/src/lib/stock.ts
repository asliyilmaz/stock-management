export class Stock {
    name: string;
    stockQty: number;
    brand: string;
    category: string;
    createdDate: any;
    updatedDate: any;

}