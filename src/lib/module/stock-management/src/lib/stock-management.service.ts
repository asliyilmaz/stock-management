import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stock } from './stock';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
// const localUrl = 'localhost:4200/assets/stock-management.json';
@Injectable({
  providedIn: 'root'
})
export class StockManagementService {

  constructor(private http: HttpClient) { }
  localUrl: string= "http://localhost:3000/Stock";
  id;
  getStocks() {
    return this.http.get<Stock[]>(this.localUrl);
  }
  postStocks(data){
    return this.http.post(this.localUrl,data).toPromise();
  }
  getSpecificId(event){
    this.id=this.http.get<Stock[]>(this.localUrl + '/' + event);
    return this.http.get<Stock[]>(this.localUrl + '/' + event);
  }
  updateStocks(data){
    return this.http.put(this.localUrl+ '/' + data['id'],data).toPromise().then((data:any)=>{
      this.getStocks();
    })
  }
}


