/*
 * Public API Surface of stock-management
 */

export * from './lib/stock-management.service';
export * from './lib/stock-management.component';
export * from './lib/stock-management.module';
